# qubes-barrier-setup

This repo holds the scripts and instructions for recreating my Qubes + Barrier kvm switch setup


make sure .sh scripts are executable! chmod +x 


some commands:

test-barrier-kvm
./barriers --screen-change-script ./ryan-screen-change-script.sh --config ./barrier.conf --debug DEBUG2 --restart --log ./log.log --no-daemon --no-tray --name test-barrier-kvm --enable-crypto --address :24800

dom0
qvm-run sys-usb 'sudo /home/user/virtual-unplug-replug-usb.sh'
