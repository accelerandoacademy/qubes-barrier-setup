#! /bin/bash

# find Barrier window
BARRIERWINDOW=$(xdotool search --name "BARRIER TELEPORTER")

# If no window found, start BARRIER TELEPORTER
[[ ! -z $BARRIERWINDOW  ]] && echo "BARRIER TELEPORTER EXISTS AND IS RUNNING"  || qvm-run -q -a --service -- test-barrier-kvm qubes.StartApp+barrier


MOUSE=$(qvm-usb | grep Logitech_USB_Optical_Mouse | cut -d' ' -f1)
KEYBOARD=$(qvm-usb | grep CHICONY_HP_Basic_USB_Keyboard | cut -d' ' -f1)

# add logic to select device names from qvm-usb list instead of hardcoded
qvm-usb attach test-barrier-kvm $MOUSE
qvm-usb attach test-barrier-kvm $KEYBOARD

# record current focus now so it can be returned later



# bring Barrier window to current desktop and give it focus, activate it, send it our hotkey
xdotool search --name "BARRIER TELEPORTER" set_desktop_for_window $(xdotool get_desktop) windowactivate windowfocus --sync key --window 0 "ctrl+2"

# return focus to where it came from
