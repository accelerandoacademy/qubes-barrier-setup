#! /bin/bash

# cat /proc/bus/input/devices

MOUSE=$(lsusb | grep "Logitech, Inc. M105 Optical Mouse" | cut -d ' ' -f 6 | xargs -L 1 /home/user/usb2sys.sh | grep -o '[^/]*$')
echo "Mouse" $MOUSE
KEYBOARD=$(lsusb | grep "HP, Inc KU-0316 Keyboard" | cut -d ' ' -f 6 | xargs -L 1 /home/user/usb2sys.sh | grep -o '[^/]*$')
echo "Keyboard" $KEYBOARD

echo $MOUSE > /sys/bus/usb/devices/$MOUSE/driver/unbind
echo $KEYBOARD > /sys/bus/usb/devices/$KEYBOARD/driver/unbind

echo $MOUSE > /sys/bus/usb/drivers/usb/bind
echo $KEYBOARD > /sys/bus/usb/drivers/usb/bind






