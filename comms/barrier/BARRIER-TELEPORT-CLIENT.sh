#!/bin/bash


# /usr/bin/gnome-terminal --title 'BARRIER TELEPORT CLIENT' --command '/home/ryan/projects/barrier/build/bin/barrierc --debug DEBUG2 --name ryan-work --restart --log /home/ryan/projects/barrier/build/bin/log.log --enable-crypto --no-daemon 192.168.0.43:24800'


echo "Initiating barrier teleportation client"

# /home/ryan/projects/barrier/build/bin/barrierc --debug DEBUG2 --name ryan-work --restart --log log.log --enable-crypto --no-daemon 192.168.0.43:24800


SYSNETIP=$(dig sys-net.kvm.teleport +short) ;
echo $SYSNETIP ;
/usr/bin/gnome-terminal --title 'BARRIER TELEPORT CLIENT' --command "/home/ryan/projects/barrier/build/bin/barrierc --debug DEBUG2 --name ryan-work --restart --log /home/ryan/projects/barrier/build/bin/log.log --enable-crypto --no-daemon $SYSNETIP:24800" ;


# How can we make this a trayicon?!
