#! /bin/bash

# This script is given a 'screen' name a an argument
# we want to run a script on sys-usb when we switch screens back to this computer, MISSION CONTROL! aka test-barrier-kvm

args=("$@")
printf "%s\n" "${args[@]}"

if [ $@ = "test-barrier-kvm" ]
   then
        qrexec-client-vm sys-usb test.resetUSB >/dev/null 2>&1 # This sends a message to dom0 to be relayed to sys-usb to run the USBreset script .sh
        # echo "So close!"
        # play -q -n synth 0.01 sin 440 >/dev/null 2>&1
        # play -q -n synth 0.01 sin 660 >/dev/null 2>&1
        play -q -n synth 0.01 sin 880 >/dev/null 2>&1
   else
        # Testing confirmation this script runs at all
        # play -q -n synth 0.01 sin 770 >/dev/null 2>&1
        play -q -n synth 0.01 sin 550 >/dev/null 2>&1
        # echo "we did it yay"
fi

